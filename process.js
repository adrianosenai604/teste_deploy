const { createApp } = Vue;

createApp({
  data() {
    return {
      username: "",
      password: "",
      error: null,
      sucesso: null,
      userAdmin: false,
      mostrarEntrada: false,

      //Utiizando Arrays para armazenamentos usuários e senhas
      usuarios: ["admin", "adriano", "julia"],
      senhas: ["1234", "12345", "123456"],

      newUsername: "",
      newPassword: "",
      confirmPassword: "",
      mostrarLista: "",
    };
  }, //Fechamento data

  methods: {
    login() {
      this.mostrarEntrada = false;
      setTimeout(() => {
        // alert("Dentro do setTimeout!");
        this.mostrarEntrada = true;
        if (
          (this.username === "Adriano" && this.password === "12342023") ||
          (this.username === "Julia" && this.password === "12345678") ||
          (this.username === "Admin" && this.password === "1234")
        ) {
          //alert("Login efetuado com sucesso!");

          localStorage.setItem("username", this.username);
          localStorage.setItem("password", this.password);

          this.error = null;
          this.sucesso = "Login efetuado com sucesso!";
          if (this.username === "Admin") {
            this.userAdmin = true;
          }
        } else {
          // alert("Login não efetuado!");
          this.error = "Nome ou senha incorretos!";
          this.sucesso = null;
        }
      }, 1000);
      // alert("Saiu do setTimeout!!!");
    }, //Fechamento Login

    login2() {
      this.mostrarEntrada = false;

      //atualização dos vetores com usuários cadastrados no sistema
      if (localStorage.getItem("usuarios") && localStorage.getItem("senhas")) {
        this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
        this.senhas = JSON.parse(localStorage.getItem("senhas"));
      }

      setTimeout(() => {
        this.mostrarEntrada = true;

        const index = this.usuarios.indexOf(this.username);
        if (index !== -1 && this.senhas[index] === this.password) {
          this.error = null;
          this.sucesso = "Login efetuado com sucesso!";

          localStorage.setItem("username", this.username);
          localStorage.setItem("password", this.password);

          if (this.username === "admin" && index === 0) {
            this.userAdmin = true;
            this.sucesso = "Logado como ADMIN!!!";
          }
        } //Fim do if
        else {
          this.sucesso = null;
          this.error = "Usuário ou senha incorretos!";
        }
      }, 1000);
    }, //Fechamento login2

    paginaCadastro() {
      this.mostrarEntrada = false;
      if (this.userAdmin == true) {
        this.error = null;
        this.sucesso = "Carregando Cadastro!";
        this.mostrarEntrada = true;
        setTimeout(() => {}, 2000);

        setTimeout(() => {
          window.location.href = "paginaCadastro.html";
        }, 1000);
      } else {
        this.sucesso = null;
        setTimeout(() => {
          this.mostrarEntrada = true;
          this.error = "Sem privilégios ADM";
        }, 1000);
      }
    }, //Fechamento paginaCadastro

    adicionarUsuario() {
      this.mostrarEntrada = false;

      this.username = localStorage.getItem("username") || "";
      this.password = localStorage.getItem("password") || "";

      setTimeout(() => {
        this.mostrarEntrada = true;

        if (this.username === "admin") {
          /*Verificando se o novo usuário é diferente de vazio e se 
            ele já não está cadastrado no sistema*/
          if (
            !this.usuarios.includes(this.newUsername) &&
            this.newUsername !== ""
          ) {
            if (this.newPassword && this.newPassword === this.confirmPassword) {
              this.usuarios.push(this.newUsername);
              this.senhas.push(this.newPassword);
              this.newUsername = "";
              this.newPassword = "";

              // Armazena os usuários no localStorage
              localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
              localStorage.setItem("senhas", JSON.stringify(this.senhas));

              this.error = null;
              this.sucesso = "Usuário cadastrado com sucesso!";
            } else {
              this.sucesso = null;
              this.error = "Por favor, confirme sua senha!";
            } //Fechamento else
          } else {
            this.sucesso = null;
            this.error = "Usuário inválido ou já existe!";
          } //Fechamento else
        } else {
          this.sucesso = null;
          this.error = "Usuário não é Administrador!";
        } //Fechamento else
      }, 1000);
    }, //Fechamento adicionarUsuario

    verCadastrados() {
      if (localStorage.getItem("usuarios") && localStorage.getItem("senhas")) {
        this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
        this.senhas = JSON.parse(localStorage.getItem("senhas"));
      }
      this.mostrarLista = !this.mostrarLista;
    }, //Fechamento vercadastrados

    excluirUsuario(usuario) {
      const index = this.usuarios.indexOf(usuario);
      if (index !== -1) {
        this.usuarios.splice(index, 1);
        this.senhas.splice(index, 1);

        // Atualiza os usuários no localStorage
        localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
        localStorage.setItem("senhas", JSON.stringify(this.senhas));
      } //Fechamento if
    }, //Fechamento excluirUsuario

    excluirUsuario2(usuario) {
      this.mostrarEntrada = false;
      if (usuario === "admin") {
        // Impedir a exclusão do usuário "admin"
        setTimeout(() => {
          this.mostrarEntrada = true;
          this.sucesso = null;
          this.error = "O usuário admin não pode ser excluído!!!";
        }, 1000);
        return;
      }//Fechamento if

      if (confirm("Tem certeza que deseja excluir o usuário?")) {
        const index = this.usuarios.indexOf(usuario);
        if (index !== -1) {
          this.usuarios.splice(index, 1);
          this.senhas.splice(index, 1);

          // Atualiza os usuários no localStorage
        localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
        localStorage.setItem("senhas", JSON.stringify(this.senhas));
        }
      }//Fechamento if
    },//Fechamento excluirUsuario2
  }, //Fechamento methods
}).mount("#app");
